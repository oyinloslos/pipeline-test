var chai = require("chai"),
	sinon = require("sinon"),
	extend = require("./extend.js"),
	assert = chai.assert,
	expect = chai.expect;

describe("#extend-class", function() {

	it("should throw error if parameters are not passed", function() {

		expect(function() {
			extend();
		}).to.throw(Error);
	});

	it("should expect o to have properties of x and z", function() {
		var o = {a:1, b:2},
			x = {c:3, d:4};
			y = 24567;
			z = {pelumi: "pelumi"};

		extend(o,x,y,z);
		expect(o).to.have.property("c");
		expect(o).to.not.have.property(24567);
		expect(o).to.have.property('pelumi');
	});
})


